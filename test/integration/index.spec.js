import SessionManager,{SessionManagerConfig} from '../../src/index';

const sessionManagerConfig = new SessionManagerConfig(
        'https://identity-service-dev.precorconnect.com',
        'https://portal.ssodev.precor.com/IdPServlet?idp_id=9lcgfn8a3xk0',
        'https://dev.precorconnect.com/customer/account/logout/'
    );

describe('Index module', () => {
    describe('default export', () => {
        it('should be SessionManager class constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new SessionManager(sessionManagerConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(SessionManager));
        });
    });
});

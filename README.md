## Description
Precor Connect session manager for browsers.

## Features

##### Get Access Token
* [documentation](features/GetAccessToken.feature)

##### Get User Info
* [documentation](features/GetUserInfo.feature)

##### Login
* [documentation](features/Login.feature)

##### Logout
* [documentation](features/Logout.feature)

## Setup  

**install via jspm**  
```shell
jspm install session-manager=bitbucket:precorconnect/session-manager-for-browsers
``` 

**import & instantiate**
```js
import SessionManager,{SessionManagerConfig} from 'session-manager';

const sessionManagerConfig =
    new SessionManagerConfig(
        'https://api-dev.precorconnect.com/'/* precorConnectApiBaseUrl */,
        'https://portal.ssodev.precor.com/IdPServlet?idp_id=9lcgfn8a3xk0'/* loginUrl */,
        'https://dev.precorconnect.com/customer/account/logout/'/* logoutUrl*/,
        3000/* accessTokenRefreshInterval */
    );
    
const sessionManager = new SessionManager(sessionManagerConfig);
```

## Platform Support

This library can be used in the **browser**.
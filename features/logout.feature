Feature: Logout
  Logs out the currently logged in user.

  Scenario: Success
    When I execute logout
    Then any accessToken in browser storage is cleared
    And the browser is redirected to the configured logoutUrl
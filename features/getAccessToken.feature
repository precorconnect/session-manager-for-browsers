Feature: Get access token
  Retrieves the accessToken of the active session. If an active session doesn't exist, initiates a login flow.
  Primarily used to build an Authorization header to make an API call to a protected resource.

  Scenario: Not logged in
    Given I am not logged in
    When I execute getAccessToken
    Then a login flow is initiated

  Scenario: Logged in
    Given I am logged in
    When I execute getAccessToken
    Then an accessToken is returned
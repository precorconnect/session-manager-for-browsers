Feature: Login
  Logs a user in

  Scenario: Success
    When I execute login
    Then the browser is redirected to the configured loginUrl
    And upon entering credentials, the browser is redirected back with an access token in the url